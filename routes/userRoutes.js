const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const UserController = require('../controllers/UserController.js');



// User registration
router.post('/signup', (request, response) => {
    UserController.registerUser(request, response);
});

// Login user || User authentication
router.post('/login', (request, response) => {
    UserController.loginUser(request, response);
});

// Retrieve user details
router.get('/retrieveUserDetails', auth.verify, (request, response) => {
    UserController.retrieveUserDetails(request, response);
});

// Admin registration
router.post('/admin-signup', (request, response) => {
    UserController.adminReg(request, response).then((result) => {
        response.send(result);
    });
});

// Setting user as admin
router.patch('/:_id', (request, response) => {
    UserController.updateUserType(request, response).then((result) => {
        response.send(result);
    });
});

module.exports = router;