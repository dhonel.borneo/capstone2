const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const OrderController = require('../controllers/OrderController.js');

//Non-admin User checkout (Create Order)
/*router.post('/create/:_id', (request, response) => {
	OrderController.createOrder(request, response);
})

// Retrieve authenticated user’s orders
router.get('/yourorders', auth.verify, (request, response) => {
	OrderController.userOrders(request, response);
})

// Retrieve all orders (Admin only)
router.get('/', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.allOrders(request, response);
})
*/
// Creating order
/*router.post('/create/:_id', auth.verify, OrderController.createOrder);*/
router.post('/create/:_id', (request, response) => {
	OrderController.createOrder(request, response);
})

// Retrieving authenticated user's order
//router.get('/userOrders', auth.verify, OrderController.getOrders);
router.get('/userOrders', auth.verify, (request, response) => {
	OrderController.userOrders(request, response);
})

// Retrieving all orders
/*router.get('/', auth.verify, auth.verifyAdmin, OrderController.getallOrders);
*/
router.get('/', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getallOrders(request, response);
})

module.exports = router;