const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');



// create product (Admin only)
/*router.post('/create', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.createProduct(request, response)
});*/
//router.post('/createProduct', auth.verify, auth.verifyAdmin, ProductController.createProduct);
router.post('/createProduct', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.createProduct(request, response)
});

// retrieve all products
/*router.get('/all', (request, response) => {
	ProductController.retrieveAllProducts(request, response);
})*/
//router.get('/getAllProducts', ProductController.retrieveAllProducts);
router.get('/getAllProducts', (request, response) => {
	ProductController.retrieveAllProducts(request, response);
})

// retrieve all active products
/*router.get('/', (request, response) => {
	ProductController.retrieveAllActiveProducts(request, response);
})*/
//router.get('/activeProducts', ProductController.retrieveAllActiveProducts);
router.get('/activeProducts', (request, response) => {
	ProductController.retrieveAllActiveProducts(request, response);
})

// retrieve single product
/*router.get('/:_id', (request, response) => {
	ProductController.retrieveProduct(request, response);
})
*/
//router.get('/:_id', ProductController.retrieveProduct);
router.get('/:_id', (request, response) => {
	ProductController.retrieveProduct(request, response);
})

// Updating specific product
//router.patch('/update/:_id', auth.verify, ProductController.updateProduct);
// Archiving specific product
//router.patch('/archive/:_id', auth.verify, ProductController.archiveProduct);
// Activating specific product
//router.patch('/activate/:_id', auth.verify, ProductController.activateProduct);



// updating a product || Update Product information (Admin only)
router.patch('/update/:_id',auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

// archiving product (Admin only)
router.patch('/archive/:_id',auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);
})

// activate product (Admin only)
router.patch('/activate/:_id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})


module.exports = router;