const express = require('express');
const CartController = require('../controllers/CartController.js');
const auth = require('../auth.js');

const router = express.Router();

// Add products to cart
/*router.post('/add/:_id', auth.verify, (request, response) => {
	CartController.createOrder(request, response);
})*/
//router.post('/add/:_id', auth.verify, CartController.createOrder);
router.post('/add/:_id', auth.verify, (request, response) => {
	CartController.createOrder(request, response);
})

module.exports = router;