const mongoose = require('mongoose');


// Cart Schema
const cart_schema = new mongoose.Schema({
	userId: {
		type: Object,
		required: true
	},
	products: [{
		productId: {
			type: Object,
			required: true
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	subTotal: {
		type: Number,
		required: true
	},
	totalAmount: {
		type: Number,
		required: true
	}
});

module.exports = mongoose.model('Cart', cart_schema);