const mongoose = require('mongoose');

// Order Schema
const order_schema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	products: [{
		productId: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Product',
			required: true
		},
		quanity : {
	        type : Number,
	    	default: 1
	    }
	}],
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn : {
        type : Date,
        default : new Date()
    }
})

module.exports = mongoose.model('Order', order_schema);