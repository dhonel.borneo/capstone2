const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'User first name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'User last name is required!']
	},
	email: {
		type: String,
		required: [true, 'User email is required!']
	},
	password: {
		type: String,
		required: [true, 'User password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNumber: {
		type: String,
		required: [true, 'User mobile number is required!']
	}
});

const Users = mongoose.model('User', userSchema);

module.exports = Users;