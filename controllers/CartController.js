const Order = require('../models/Order.js');
const User = require('../models/User.js');
const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');
const auth = require('../auth.js');



// Add products to cart
module.exports.addProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;

	Product.findById(request.params._id)
	.then(result => {
		let newCart = new Cart({
			userId: userData.id,
			products: [{
				productId: request.params._id,
			}],
			subTotal: result.price,
			totalAmount: result.price
		});

		newCart.save()
		.then(save => response.send('Succesfully added to cart'))
		.catch(error => response.send(error));
	})
	.catch(error => response.send(errr));
};