const Order = require('../models/Order.js');
const User = require('../models/User.js');
const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');
const auth = require('../auth.js');

module.exports.createProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let newProduct = new Product({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		});

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false);
	};
}

module.exports.retrieveAllProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Product.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
	else{
		return response.send(error);
	};
}

module.exports.retrieveAllActiveProducts = (request, response) => {
	Product.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


module.exports.retrieveProduct = (request, response) => {
	const productId = request.params._id;

	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	if(userData.isAdmin){
		Product.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false)
	}
}

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let archivedProduct = {
		isActive: false
	};

	if(userData.isAdmin){
		Product.findByIdAndUpdate(productId, archivedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		 return response.send(false);
	};
}

module.exports.activateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	let activatedProduct = {
		isActive: true
	};

	if(userData.isAdmin){
		Product.findByIdAndUpdate(productId, activatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else{
		return response.send(false);
	};
}