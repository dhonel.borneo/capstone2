
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

const Order = require('../models/Order.js');
const User = require('../models/User.js');
const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');


module.exports.registerUser = (request, response) => {
/*	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));*/

	User.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNumber: request.body.mobileNumber
			});

			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		}
		else{
			return response.send(false);
		};
	})
	.catch(error => response.send(false));

}

// Admin registration
module.exports.adminReg = (request, response) => {
	User.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			let newUser = new Users({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin
			});

			newUser.save()
			.then(saved => response.send(`${request.body.email} is now registered!`))
			.catch(error => response.send(error));
		}
		else{
			return response.send(`${request.body.email} is already taken. Try logging in or use different email in signing up!`);
		};
	})
	.catch(error => response.send(error));
};

module.exports.loginUser = (request, response) => {
	/*return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			}) 
		}

		// If a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// If the password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({
				accessToken: auth.createAccessToken(result),
				userId: result._id 
			});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));*/

	User.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false);
		}
		else{
			let isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			
			if(isPasswordCorrect){
				return response.send({
					token: auth.createAccessToken(result)
				});
			}
			else{
				return response.send(false);
			};
		};
	})
	.catch(error => response.send(false));
}

module.exports.retrieveUserDetails = (request, response) => {
/*		return User.findOne({_id: request.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";

			return user;
		}).catch(error => console.log(error));*/


	const userData = auth.decode(request.headers.authorization);

	User.findOne({_id: userData.id})
	.then(data => response.send(data));	
}


module.exports.userAdmin = (request, response) => {
    const user_Data = auth.decode(request.headers.authorization);
	const userId = request.params._id;
	let updated_User = {
		isAdmin: request.body.isAdmin
	};

	if(user_Data.isAdmin){
		User.findByIdAndUpdate(userId, updated_User)
		.then(result => response.send('Successfully set user as admin!'))
		.catch(error => response.send(error));
	}
	else{
		return response.send('Denied')
	};
}


// Setting user as admin
module.exports.updateUserType = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params._id;
	let updatedUser = {
		isAdmin: request.body.isAdmin
	};

	if(userData.isAdmin){
		User.findByIdAndUpdate(userId, updatedUser)
		.then(result => response.send('You have successfully set user as admin!'))
		.catch(error => response.send(error));
	}
	else{
		return response.send('You do not have permission to set user as admin!')
	};
};


